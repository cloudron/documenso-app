### About

Signing documents digitally should be fast and easy and should be the best practice for every document signed worldwide. This is technically quite easy today, but it also introduces a new party to every signature: The signing tool providers. While this is not a problem in itself, it should make us think about how we want these providers of trust to work. Documenso aims to be the world's most trusted document-signing tool. This trust is built by empowering you to self-host Documenso and review how it works under the hood.

### Features

* Fast. When it comes to sending or receiving a contract, you can count on lightning-fast speeds.
* Beautiful. Because signing should be celebrated. That’s why we care about the smallest detail in our product.
* Smart. Our custom templates come with smart rules that can help you save time and energy.
* Easy Sharing (Soon). Receive your personal link to share with everyone you care about.
* Connections. Create connections and automations with Zapier and more to integrate with your favorite tools.
* Get paid (Soon). Integrated payments with Stripe so you don’t have to worry about getting paid.
* React Widget (Soon). Easily embed Documenso into your product. Simply copy and paste our react widget into your application.

