#!/usr/bin/env node

'use strict';

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    const ADMIN_EMAIL = 'admin@cloudron.local';
    const ADMIN_PASSWORD = 'changeme';
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(email, password) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/signin');

        await waitForElement(By.xpath('//input[@name="email"]'));

        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys(email);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//button[contains(., "Sign In")]')).click();
        await browser.sleep(3000);
        await waitForElement(By.xpath('//h1[contains(., "Documents")]'));
    }

    let hasOIDCSession = false;
    async function loginOIDC(username, password) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/signin`);

        await waitForElement(By.xpath('//button[contains(., "OIDC")]'));
        await browser.findElement(By.xpath('//button[contains(., "OIDC")]')).click();

        if (!hasOIDCSession) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();

            hasOIDCSession = true;
        }

        await waitForElement(By.xpath('//h1[contains(., "Documents")]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[@data-testid="menu-switcher"]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//div[@role="menuitem" and contains(text(), "Sign Out")]')).click();
        await waitForElement(By.xpath('//input[@name="email"]'));
    }

    async function uploadPDF() {
        await browser.get('https://' + app.fqdn + '/documents');
        await browser.sleep(2000);

        await waitForElement(By.xpath(`//p[text()="Add a document"]`));

        await browser.findElement(By.xpath('//input[@type="file"]')).sendKeys(path.resolve(__dirname, './pdf-sample.pdf'));
        await browser.sleep(2000);

        await browser.get('https://' + app.fqdn + '/documents');
        await browser.sleep(2000);

        await waitForElement(By.xpath('//a[@title="pdf-sample.pdf"]'));
    }

    async function checkPDF() {
        await browser.get('https://' + app.fqdn + '/documents');
        await browser.sleep(2000);
        await waitForElement(By.xpath('//a[@title="pdf-sample.pdf"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no sso
    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can upload PDF', uploadPDF);
    it('check PDF', checkPDF);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // sso
    it('install app (sso)', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can upload PDF', uploadPDF);
    it('check PDF', checkPDF);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login', loginOIDC.bind(null, username, password));
    it('check PDF', checkPDF);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', loginOIDC.bind(null, username, password));
    it('check PDF', checkPDF);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can login', loginOIDC.bind(null, username, password));
    it('check PDF', checkPDF);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app for update', function () { execSync(`cloudron install --appstore-id com.documenso.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can upload PDF', uploadPDF);
    it('check PDF', checkPDF);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('check PDF', checkPDF);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
