FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4 AS base

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code


FROM base AS builder

RUN apt-get update \
    && apt-get install  -y --no-install-recommends libc6 \
    && apt-get clean \
    && rm -rf /var/cache/apt /var/lib/apt/lists

# renovate: datasource=github-releases depName=documenso/documenso versioning=semver extractVersion=^v(?<version>.+)$
ENV DOCUMENSO_VERSION=1.9.0

RUN curl -SLf "https://github.com/documenso/documenso/archive/refs/tags/v${DOCUMENSO_VERSION}.tar.gz" | tar xz --strip-components 1 -C /app/code

RUN npm ci

ENV NODE_ENV=production
ENV NEXT_TELEMETRY_DISABLED=1
ENV HUSKY=0
ENV DOCKER_OUTPUT=1

# will be replaced in start.sh
ENV NEXT_PUBLIC_WEBAPP_URL=http://NEXT_PUBLIC_WEBAPP_URL_PLACEHOLDER

# Encryption keys
ENV NEXT_PRIVATE_ENCRYPTION_KEY="CAFEBABE"
ENV NEXT_PRIVATE_ENCRYPTION_SECONDARY_KEY="DEADBEEF"

RUN npm install -g "turbo@^1.9.3"
RUN turbo run build --filter=@documenso/web...


FROM base AS runner

# TODO keep in sync with https://github.com/documenso/documenso/blob/main/docker/Dockerfile#L87
COPY --from=builder /app/code/apps/web/next.config.js .
COPY --from=builder /app/code/apps/web/package.json .

# Automatically leverage output traces to reduce image size
# https://nextjs.org/docs/advanced-features/output-file-tracing
COPY --from=builder /app/code/apps/web/.next/standalone ./
COPY --from=builder /app/code/apps/web/.next/static ./apps/web/.next/static
COPY --from=builder /app/code/apps/web/public ./apps/web/public
COPY --from=builder /app/code/apps/web/.next/BUILD_ID ./apps/web/.next/BUILD_ID

# Copy the prisma binary, schema and migrations
COPY --from=builder /app/code/packages/prisma /app/code/packages/prisma
COPY --from=builder /app/code/node_modules/prisma/ ./node_modules/prisma/
COPY --from=builder /app/code/node_modules/@prisma/ ./node_modules/@prisma/

# Symlink the prisma binary
RUN mkdir node_modules/.bin
RUN ln -s /app/code/node_modules/prisma/build/index.js ./node_modules/.bin/prisma

# Copy modules for data seeding
COPY --from=builder /app/code/node_modules/nanoid/ ./node_modules/nanoid/
COPY --from=builder /app/code/node_modules/@documenso/lib/server-only/auth/ ./node_modules/@documenso/lib/server-only/auth/
COPY --from=builder /app/code/node_modules/@documenso/lib/constants/ ./node_modules/@documenso/lib/constants/
COPY --from=builder /app/code/node_modules/@documenso/prisma ./node_modules/@documenso/prisma
COPY --from=builder /app/code/assets/ /app/code/assets
COPY --from=builder /app/code/apps/web/example/cert.p12 /app/pkg/cert.p12

RUN mv /app/code/apps/web/.next /app/code/apps/web/.next.orig && \
    mv /app/code/apps/web/public /app/code/apps/web/public.orig && \
    ln -sf /run/documenso/env /app/code/apps/web/.env && \
    ln -sf /app/data/resources /app/code/resources

# put our db seed script
COPY --from=builder /app/code/packages/prisma/seed/initial-seed.ts /app/pkg/initial-seed.ts.orig
COPY initial-seed.ts /app/code/packages/prisma/seed/initial-seed.ts

COPY start.sh env.template /app/pkg/

CMD [ "/app/pkg/start.sh" ]
