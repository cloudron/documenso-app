[0.1.0]
* Initial version for Documenso 1.5.6

[0.2.0]
* Fix manifest and marketing url

[0.3.0]
* Update Documenso to 1.6.1
* [Full changelog](https://github.com/documenso/documenso/releases/tag/v1.6.1)

[0.4.0]
* Update documenso to 1.7.1
* [Full Changelog](https://github.com/documenso/documenso/releases/tag/v1.7.1)
* fix: refactor dates by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1321
* feat: marketing cta by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1335
* fix: select field ux by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1334
* fix: prefill advanced field settings with current field meta in template by [@&#8203;Etrenak](https://github.com/Etrenak) in https://github.com/documenso/documenso/pull/1332
* feat: add authOptions to the API by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1338
* fix: template with empty advanced fields by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1339
* fix: template with empty advanced fields backend by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1340
* feat: add language switcher by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1337

[0.5.0]
* when sso enabled, disable external sign up

[0.6.0]
* Update documenso to 1.7.2
* [Full Changelog](https://github.com/documenso/documenso/releases/tag/v1.7.2)
* chore: add translations by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1354
* feat: add french by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1355
* feat: allow editing pending documents by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1346
* fix: set lang cookie expiry by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1365
* chore: prisma customer story on blog by [@&#8203;ElTimuro](https://github.com/ElTimuro) in https://github.com/documenso/documenso/pull/1366
* fix: dateformat bug by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1372
* chore: field fonts by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1350

[1.0.0]
* First stable package release with documenso 1.7.2

[1.0.1]
* Update documenso to 1.8.0
* [Full Changelog](https://github.com/documenso/documenso/releases/tag/v1.8.0)
* feat: add template page by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1395
* feat: add certificate translations by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1440
* fix: checkout loading button by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1445
* feat(email): support configurable SMTP service by [@&#8203;Jorgagu](https://github.com/Jorgagu) in https://github.com/documenso/documenso/pull/1447
* fix: update docker environment by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1438
* feat: add signing link copy by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1449
* fix: custom team email subject by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1450
* fix: email translations by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1454
* feat: add document distribution setting by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1437
* feat: add global settings for teams by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1391
* fix: fix the document url in the command menu search by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1453
* fix: certificate translations by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1460
* fix: update publish workfow to only tag stable versions latest by [@&#8203;david-loe](https://github.com/david-loe) in https://github.com/documenso/documenso/pull/1405
* fix: errors moving fields by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1429
* feat: signature rejection by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1472
* fix: always allow access to billing by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1476
* feat: support windows for 2fa tokens by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1478
* fix: sort recipients for template with signing order by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1468

[1.0.2]
* Update documenso to 1.8.1
* [Full Changelog](https://github.com/documenso/documenso/releases/tag/v1.8.1)
* feat: automatically sign fields in large documents by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1484
* fix: improve field sizing by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1486
* fix: hardcode delete confirmation text to avoid translation mismatch by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1487
* feat: support whitelabelling in the embedding by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1491
* feat: download doc without signing certificate by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1477
* fix: document title truncation by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1467
* feat: add initial api logging by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1494
* fix: wrong signing invitation message by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1497
* feat: signing volume by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1358
* fix: auth cookies across iframes by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1501
* feat: add platform plan pricing by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1505
* fix: dateformat api bug by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1506
* fix: docs content by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1495
* feat: upload signature as img by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1496
* [@&#8203;luhagel](https://github.com/luhagel) made their first contribution in https://github.com/documenso/documenso/pull/1507

[1.1.0]
* Update documenso to 1.9.0
* [Full Changelog](https://github.com/documenso/documenso/releases/tag/v1.9.0)
* Update README.md by [@&#8203;ElTimuro](https://github.com/ElTimuro) in https://github.com/documenso/documenso/pull/1509
* fix: refactor teams router by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1500
* feat: return fields in GET /documents/:id endpoint by [@&#8203;Etrenak](https://github.com/Etrenak) in https://github.com/documenso/documenso/pull/1317
* fix: refactor trpc errors by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1511
* fix: prevent accidental signatures by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1515
* fix: refactor routers by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1523
* feat: open page api by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1419
* fix: refactor search routes by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1529
* fix: add billing leeway by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1532
* fix: secure passkey cookies by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1533
* feat: add controls for sending completion emails to document owners by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1534
* fix: clear invalid drawn signature when switching to typed signature by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1536
* fix: prevent hidden layers from being toggled in pdf viewers by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1528
* fix: admin leaderboard query by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1522
* fix: darkmode on radio button and checkbox labels  by [@&#8203;doug-andrade](https://github.com/doug-andrade) in https://github.com/documenso/documenso/pull/1518
* feat: add trpc openapi by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1535
* feat: make enterprise billing dynamic by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1539
* fix: document visibility logic by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1521
* fix: move permission check outside the document visibility component by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1543
* fix: fieldtooltip blocking the field click by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1538
* fix: z-index of field settings by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1469
* feat: add disabled property for user by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1546
* fix: checkbox logic by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1537
* fix: tests by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1556
* fix: billing page formatting by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1554
* feat: add get recipient route by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1553
* fix: remove unwanted semicolon by [@&#8203;SYNO-SY](https://github.com/SYNO-SY) in https://github.com/documenso/documenso/pull/1545
* feat: notify owner when a recipient signs by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1549
* fix: make small fields draggable by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1551
* feat: additional valid password by [@&#8203;Dun-sin](https://github.com/Dun-sin) in https://github.com/documenso/documenso/pull/1456
* feat: enable optional fields by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1470
* feat: open the advanced settings automatically by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1508
* fix: avoid having a drawn and typed signature at the same time by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1516
* feat: admin ui for disabling users by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1547
* fix: default to user timezone by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1559
* fix: remove marketing by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1562
* fix: merge common and web po files by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1563
* fix: label stable releases as latest by [@&#8203;david-loe](https://github.com/david-loe) in https://github.com/documenso/documenso/pull/1567
* feat: allow switching document when using templates by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1571
* fix: add document visibility to template by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1566
* fix: update template field schema by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1575
* feat: add template and field endpoints by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1572
* fix: broken direct template webhook by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1579
* fix: pending document edit by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1580
* feat: ignore unrecognized fields from authorization response by [@&#8203;samuelraub](https://github.com/samuelraub) in https://github.com/documenso/documenso/pull/1479
* fix: refactor prisma relations by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1581
* feat: add consistent response schemas by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1582
* feat: add prisma json types by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1583
* feat: add create document beta endpoint by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1584
* fix: bump trpc and openapi packages by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1591
* feat: add angular embedding docs by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1592
* feat: add get field endpoints by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1599
* fix: add empty success responses by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1600
* feat: copy, paste, duplicate template fields by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1594
* fix: correct redirect after document duplication by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1595
* feat: add text align option to fields by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1610
* fix: admin leaderboard query sorting by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1548
* fix: tidy document invite email render logic by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1597
* feat: bulk send templates via csv by [@&#8203;Mythie](https://github.com/Mythie) in https://github.com/documenso/documenso/pull/1578
* fix: create global settings on team creation by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1601
* feat: add Polish and Italian by [@&#8203;dguyen](https://github.com/dguyen) in https://github.com/documenso/documenso/pull/1618
* feat: assistant role by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1588
* fix: unable to check on the checkbox field by [@&#8203;ephraimduncan](https://github.com/ephraimduncan) in https://github.com/documenso/documenso/pull/1593
* fix: field label/text truncation by [@&#8203;catalinpit](https://github.com/catalinpit) in https://github.com/documenso/documenso/pull/1565

