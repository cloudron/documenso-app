#!/bin/bash
set -eu

APP_PATH=/app/code

mkdir -p /app/data/documenso /app/data/resources /run/documenso/.npm

export HOSTNAME=0.0.0.0
export PORT=3000

export DATABASE_URL="postgres://${CLOUDRON_POSTGRESQL_USERNAME}:${CLOUDRON_POSTGRESQL_PASSWORD}@${CLOUDRON_POSTGRESQL_HOST}:${CLOUDRON_POSTGRESQL_PORT}/${CLOUDRON_POSTGRESQL_DATABASE}"
export NEXT_PRIVATE_DATABASE_URL="${DATABASE_URL}"
export NEXT_PRIVATE_DIRECT_DATABASE_URL="${DATABASE_URL}"

if [[ ! -f /app/data/.nextauth_secret ]]; then
    echo "=> First run"

    echo "==> Generate NEXTAUTH secret"
    openssl rand -base64 32 > /app/data/.nextauth_secret
fi

if [[ ! -d ${APP_PATH}/apps/web/.next/static || ! $(grep -rl "${CLOUDRON_APP_ORIGIN}" ${APP_PATH}/apps/web/.next) ]]; then
    echo "=> Copying .next resources"
    cp -R ${APP_PATH}/apps/web/.next.orig/. ${APP_PATH}/apps/web/.next
fi

if [[ ! -d ${APP_PATH}/apps/web/public/static ]]; then
    echo "=> Copying public resources"
    cp -R ${APP_PATH}/apps/web/public.orig/. ${APP_PATH}/apps/web/public
fi

if [[ ! -f /app/data/.next_private_encryption_key ]]; then
    echo "==> Generate NEXT PRIVATE ENCRYPTION KEY"
    openssl rand -base64 32 > /app/data/.next_private_encryption_key
fi
if [[ ! -f /app/data/.next_private_encryption_secondary_key ]]; then
    echo "==> Generate NEXT PRIVATE ENCRYPTION SECONDARY KEY"
    openssl rand -base64 32 > /app/data/.next_private_encryption_secondary_key
fi

echo "==> Create / Migrate db"
cd ${APP_PATH} && npx --cache /run/documenso/.npm prisma migrate deploy --schema ./packages/prisma/schema.prisma

if [[ ! -f /app/data/resources/cert.p12 ]]; then
    # we use a certificate coming with documenso
    echo "=> Copy a certificate coming with documenso"
    cp /app/pkg/cert.p12 /app/data/resources/cert.p12
    echo "   NOTE: You should follow First Time Setup instructions to create your own signing certificate"
fi

if [[ ! -f /app/data/.data_feeded ]]; then
    echo "=> Import demo data"
    cd ${APP_PATH}/packages/prisma && npm_config_yes=true npx --cache /run/documenso/.npm tsx ./seed-database.ts
    touch /app/data/.data_feeded
fi

export NEXTAUTH_SECRET=$(cat /app/data/.nextauth_secret)
export NEXT_PRIVATE_ENCRYPTION_KEY=$(cat /app/data/.next_private_encryption_key)
export NEXT_PRIVATE_ENCRYPTION_SECONDARY_KEY=$(cat /app/data/.next_private_encryption_secondary_key)
export NEXT_PUBLIC_MARKETING_URL=https://documenso.com # for share cards after signing
export NEXTAUTH_URL="${CLOUDRON_APP_ORIGIN}"
export NEXT_PUBLIC_WEBAPP_URL="${CLOUDRON_APP_ORIGIN}"

echo "=> Update site config"
if [[ ! -f /app/data/env ]]; then
    if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        echo -e "NEXT_PUBLIC_DISABLE_SIGNUP=true\nNEXT_PRIVATE_OIDC_ALLOW_SIGNUP=true\nNEXT_PRIVATE_OIDC_SKIP_VERIFY=true" > /app/data/env
    else
        # enable signups
        echo "NEXT_PUBLIC_DISABLE_SIGNUP=false" > /app/data/env
    fi
fi

cat /app/pkg/env.template > /run/documenso/env

# OIDC
if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "==> Setting up OIDC"

# CLOUDRON_OIDC_PROVIDER_NAME is not supported
    cat >> /run/documenso/env <<EOT

NEXT_PRIVATE_OIDC_WELL_KNOWN=${CLOUDRON_OIDC_DISCOVERY_URL}
NEXT_PRIVATE_OIDC_CLIENT_ID=${CLOUDRON_OIDC_CLIENT_ID}
NEXT_PRIVATE_OIDC_CLIENT_SECRET=${CLOUDRON_OIDC_CLIENT_SECRET}
EOT

fi

cat /app/data/env >> /run/documenso/env

echo "=> Migrate DB"
cd ${APP_PATH} && npx --cache /run/documenso/.npm prisma migrate deploy --schema ./packages/prisma/schema.prisma

echo "==> Update site URL"
find ${APP_PATH}/apps/web/.next/ ${APP_PATH}/apps/web/public -type f | while read file; do
    sed -i "s,http://NEXT_PUBLIC_WEBAPP_URL_PLACEHOLDER,${CLOUDRON_APP_ORIGIN},g" "$file"
done

echo "==> Changing ownership"
chown -R cloudron:cloudron /run/documenso /app/data ${APP_PATH}/apps/web/public ${APP_PATH}/apps/web/.next

echo "=> Starting Documenso"
#cd /run/documenso
exec gosu cloudron node apps/web/server.js
